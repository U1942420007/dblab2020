10.Compute the average budget of the films directed by Peter Jackson.

select avg(budget) 
from movies 
where movies.movie_id in 
	(select movie_id 
	 from (movie_directors join directors on movie_directors.director_id = directors.director_id)
	 where director_name = 'Peter Jackson')
	 
11. Show the Francis Ford Coppola film that has the minimum budget.

select title, budget
from movies 
where movies.movie_id in 
	(select movie_id 
	 from (movie_directors join directors on movie_directors.director_id = directors.director_id)
	 where director_name = 'Francis Ford Coppola')
order by budget
limit 1
	 
12. Show the film that has the most vote and has been produced in USA.

select title, votes
from movies
where movies.movie_id in 
	(select movie_id 
	 from producer_countries join countries on producer_countries.country_id = countries.country_id
     where country_name = "USA")
order by votes desc
limit 1