create or replace view  usa_customer as
select CustomerID, CustomerName, ContactName
from Customers
where Country="USA";

select*
from usa_customers join orders on usa_customers.CustomersID=orders.CustomerID;

create view products_below_avg_price as
select ProductID, ProductName,Price
from Products
where Price<(select avg(Price)from Products);

select* from products_below_avg_price;